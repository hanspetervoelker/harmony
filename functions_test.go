package harmony

import (
	"fmt"
	"strconv"
	"testing"
)

func ExampleInterval_Distance() {
	root := intervals[`1`]
	minorThird := intervals[`b3`]
	majorThird := intervals[`3`]
	fmt.Printf("minor: %d, major: %d\n", root.Distance(minorThird), root.Distance(majorThird))
	fmt.Printf("minor: %d, major: %d", minorThird.Distance(root), majorThird.Distance(root))
	// Output:
	// minor: 3, major: 4
	// minor: 3, major: 4
}

func ExampleInterval_FromKey() {
	k := Key{
		Name:        `G`,
		DisplayName: `G`,
		Signs:       signs[`G`],
		KeyNotes:    keyNotes[`G`],
	}
	root := intervals[`1`].FromKey(k)
	minorThird := intervals[`b3`].FromKey(k)
	fmt.Printf("root: %s, minorThird: %s", root[0].DisplayName, minorThird[0].DisplayName)
	// Output:
	// root: G, minorThird: B♭
}

func ExampleInterval_Tonality() {
	minorThird := intervals[`b3`].Tonality()
	majorThird := intervals[`3`].Tonality()
	second := intervals[`2`].Tonality()
	fmt.Printf("Tonalities: b3: %s, 3: %s, 2: %s", minorThird, majorThird, second)
	// Output:
	// Tonalities: b3: minor, 3: major, 2: none
}

func ExampleNoteList_ToString() {
	k := Key{
		Name:        `Ab`,
		DisplayName: `Ab`,
		Signs:       signs[`Ab`],
		KeyNotes:    keyNotes[`Ab`],
	}
	minorThird := intervals[`b3`].FromKey(k)
	fmt.Println(minorThird.ToString())
	// Output:
	// [C♭]
}

func ExampleKey_Left() {
	k := HarmonyModels.createKey(`B`, Ionian)
	l := k.Left()
	r := k.Right()
	fmt.Printf("This: %s-Major(%d signs), Left: %s(%d signs), Right: %s(%d signs)\n", k.DisplayName, len(k.Signs), l.DisplayName, len(l.Signs), r.DisplayName, len(r.Signs))
	k = HarmonyModels.createKey(`F#`, Ionian)
	l = k.Left()
	r = k.Right()
	fmt.Printf("This: %s-Major(%d signs), Left: %s(%d signs), Right: %s(%d signs)\n", k.DisplayName, len(k.Signs), l.DisplayName, len(l.Signs), r.DisplayName, len(r.Signs))
	k = HarmonyModels.createKey(`G#`, Dorian)
	l = k.Left()
	r = k.Right()
	fmt.Printf("This: %s-Dorian(%d signs), Left: %s(%d signs), Right: %s(%d signs)\n", k.DisplayName, len(k.Signs), l.DisplayName, len(l.Signs), r.DisplayName, len(r.Signs))
	// Output:
	// This: B-Major(5 signs), Left: E(4 signs), Right: F♯(6 signs)
	// This: F♯-Major(6 signs), Left: B(5 signs), Right: D♭(5 signs)
	// This: G♯-Dorian(6 signs), Left: B(5 signs), Right: D♭(5 signs)
}

func ExampleKey_Relative() {
	k := HarmonyModels.createKey(`C#`, Dorian)
	rel := k.Relative()
	fmt.Println(rel.DisplayName)
	fmt.Println(k.Signs)
	k = HarmonyModels.createKey(`C`, Ionian)
	rel = k.Relative()
	fmt.Println(rel.DisplayName)
	k = HarmonyModels.createKey(`A`, Aeolian)
	rel = k.Relative()
	fmt.Println(rel.DisplayName)
	fmt.Println(rel.Signs, k.Signs)
	// Output:
	// B
	// [F# C# G# D# A#]
	// A
	// C
	// [] []
}

func ExampleKey_Parallel() {
	k := HarmonyModels.createKey(`D`, Ionian)
	p := k.Parallel()
	fmt.Printf("D-Major: NbrOfSigns: %d(%v)\n", len(k.Signs), k.Signs)
	fmt.Printf("Parallel: %s, nbrOfSigns: %d (%v), Major: %s\n", p.DisplayName, len(p.Signs), p.Signs, p.Relative().DisplayName)
	k = HarmonyModels.createKey(`A`, Aeolian)
	p = k.Parallel()
	fmt.Printf("A-Minor: NbrOfSigns: %d, Major: %s\n", len(k.Signs), k.Relative().DisplayName)
	fmt.Printf("Parallel: %s, nbrOfSigns: %d (%v)\n", p.DisplayName, len(p.Signs), p.Signs)
	// Output:
	// D-Major: NbrOfSigns: 2([F# C#])
	// Parallel: D, nbrOfSigns: 1 ([Bb]), Major: F
	// A-Minor: NbrOfSigns: 0, Major: C
	// Parallel: A, nbrOfSigns: 3 ([F# C# G#])
}

func ExampleCoF_MajorKeys() {
	cof := HarmonyModels.CreateCoF(false)
	mk := cof.MajorKeys()
	g := mk[1]
	fsharp := mk[5]
	eb := mk[9]
	fmt.Printf("%s-Major: position: %d, nbrOfSigns: %d\n", g.DisplayName, g.CirclePosition, len(g.Signs))
	fmt.Printf("%s-Major: position: %d, nbrOfSigns: %d\n", fsharp.DisplayName, fsharp.CirclePosition, len(fsharp.Signs))
	fmt.Printf("%s-Major: position: %d, nbrOfSigns: %d\n", eb.DisplayName, eb.CirclePosition, len(eb.Signs))
	// Output:
	// G-Major: position: 1, nbrOfSigns: 1
	// B-Major: position: 5, nbrOfSigns: 5
	// E♭-Major: position: 9, nbrOfSigns: 3
}

func ExampleCoF_MinorKeys() {
	cof := HarmonyModels.CreateCoF(false)
	mk := cof.MinorKeys()
	e := mk[1]
	c := mk[9]
	fmt.Printf("%s-Minor: position: %d, nbrOfSigns: %d\n", e.DisplayName, e.CirclePosition, len(e.Signs))
	fmt.Printf("%s-Minor: position: %d, nbrOfSigns: %d\n", c.DisplayName, c.CirclePosition, len(c.Signs))
	// Output:
	// E-Minor: position: 1, nbrOfSigns: 1
	// C-Minor: position: 9, nbrOfSigns: 3
}

func TestCoF_CreateCoF(t *testing.T) {
	cof := HarmonyModels.CreateCoF(true)
	mk := cof.MajorKeys()
	if mk[6].Name != `Gb` {
		t.Errorf("expected Gb but got %s", mk[6].Name)
	}
}

func TestCoF_MinorKeys(t *testing.T) {
	cof := HarmonyModels.CreateCoF(false)
	mk := cof.MinorKeys()
	if mk[0].CirclePosition != 0 && mk[0].Name != `A` {
		t.Errorf("expected A position 0 but got %s position %d", mk[5].Name, mk[5].CirclePosition)
	}
	if mk[5].CirclePosition != 5 && mk[5].Name != `Ab` {
		t.Errorf("expected Ab position 5 but got %s position %d", mk[5].Name, mk[5].CirclePosition)
	}
	if mk[11].CirclePosition != 11 && mk[11].Name != `F` {
		t.Errorf("expected F position 11 but got %s position %d", mk[11].Name, mk[11].CirclePosition)
	}
}

func ExampleKeyList_ByName() {
	var mk KeyList
	cof := HarmonyModels.CreateCoF(true)
	mk = cof.MajorKeys()
	k := mk.ByName(`Eb`)
	fmt.Printf("Name: %s, Position: %d, NbrOfSigns: %d\n", k.DisplayName, k.CirclePosition, len(k.Signs))
	var keys = cof.MajorKeys()
	k = keys.ByName(`Gb`)
	fmt.Printf("Name: %s, Position: %d, NbrOfSigns: %d", k.DisplayName, k.CirclePosition, len(k.Signs))
	// Output:
	// Name: E♭, Position: 9, NbrOfSigns: 3
	// Name: G♭, Position: 6, NbrOfSigns: 6
}

func TestKeyList_ByName(t *testing.T) {
	var mk KeyList
	cof := HarmonyModels.CreateCoF(true)
	mk = cof.MajorKeys()
	k := mk.ByName(`xx`)
	if k.Name != `` {
		t.Error("expected empty Key but got one")
	}
}

func TestKeyList_ByPosition(t *testing.T) {
	var mk KeyList
	cof := HarmonyModels.CreateCoF(true)
	mk = cof.MajorKeys()
	k := mk.ByPosition(99)
	if k.Name != `` {
		t.Error("expected empty Key but got one")
	}
}

func ExampleCoF_ChurchModeKeys() {
	cof := HarmonyModels.CreateCoF(true)
	cmk := cof.ChurchModeKeys(Dorian)
	dDorian := cmk.ByPosition(0)
	aDorian := cmk.ByPosition(1)
	gDorian := cmk.ByPosition(11)
	fmt.Printf("Name: %s-Dorian, nbrOfSigns: %d\n", dDorian.DisplayName, len(dDorian.Signs))
	fmt.Printf("Name: %s-Dorian, nbrOfSigns: %d\n", aDorian.DisplayName, len(aDorian.Signs))
	fmt.Printf("Name: %s-Dorian, nbrOfSigns: %d\n", gDorian.DisplayName, len(gDorian.Signs))
	fmt.Println(gDorian.Relative().DisplayName)
	cmk = cof.ChurchModeKeys(Lydian)
	dLydian := cmk.ByPosition(0)
	aLydian := cmk.ByPosition(1)
	gLydian := cmk.ByPosition(11)
	fmt.Printf("Name: %s-Lydian, nbrOfSigns: %d\n", dLydian.DisplayName, len(dLydian.Signs))
	fmt.Printf("Name: %s-Lydian, nbrOfSigns: %d\n", aLydian.DisplayName, len(aLydian.Signs))
	fmt.Printf("Name: %s-Lydian, nbrOfSigns: %d\n", gLydian.DisplayName, len(gLydian.Signs))
	fmt.Println(gLydian.Relative().DisplayName)
	cmk = cof.ChurchModeKeys(Lydian)
	res := cmk.ByPosition(6)
	fmt.Printf("Name: %s-Lydian, nbrOfSigns: %d\n", res.DisplayName, len(res.Signs))
	cmk = cof.ChurchModeKeys(Aeolian)
	res = cmk.ByPosition(6)
	fmt.Printf("Name: %s-Aeolian, nbrOfSigns: %d\n", res.DisplayName, len(res.Signs))
	fmt.Println(res.Relative().DisplayName)
	// Output:
	// Name: D-Dorian, nbrOfSigns: 0
	// Name: A-Dorian, nbrOfSigns: 1
	// Name: G-Dorian, nbrOfSigns: 1
	// F
	// Name: F-Lydian, nbrOfSigns: 0
	// Name: C-Lydian, nbrOfSigns: 1
	// Name: B♭-Lydian, nbrOfSigns: 1
	// F
	// Name: C♭-Lydian, nbrOfSigns: 6
	// Name: E♭-Aeolian, nbrOfSigns: 6
	// G♭
}

func ExampleCoF_Segment() {
	cof := HarmonyModels.CreateCoF(false)
	seg := cof.Segment(8)
	for _, k := range seg {
		fmt.Printf("Name: %s (%d signs), Mode: %s, Relative: %s, Parallel: %s (%d signs), pos: %d\n", k.DisplayName, len(k.Signs), k.Mode, k.Relative().DisplayName, k.Parallel().DisplayName, len(k.Parallel().Signs), k.Parallel().CirclePosition)
	}
	// Output:
	// Name: A♭ (4 signs), Mode: Ionian, Relative: F, Parallel: A♭ (5 signs), pos: 5
	// Name: B♭ (4 signs), Mode: Dorian, Relative: A♭, Parallel: B♭ (2 signs), pos: 10
	// Name: C (4 signs), Mode: Phygrian, Relative: A♭, Parallel: C (0 signs), pos: 0
	// Name: D♭ (4 signs), Mode: Lydian, Relative: A♭, Parallel: D♭ (5 signs), pos: 7
	// Name: E♭ (4 signs), Mode: Mixolydian, Relative: A♭, Parallel: E♭ (3 signs), pos: 9
	// Name: F (4 signs), Mode: Aeolian, Relative: A♭, Parallel: F (1 signs), pos: 11
	// Name: G (4 signs), Mode: Locrian, Relative: A♭, Parallel: G (1 signs), pos: 1
}

func TestMode_String(t *testing.T) {
	var m = Mode(99)
	s := m.String()
	if s != `99` {
		t.Error("expected number 99 but got different")
	}
}

func TestGetNormalizedPosition(t *testing.T) {
	var pos = 0
	var move = -1
	res := getNormalizedPosition(pos, move)
	if res < 0 || res > 11 {
		t.Error("normalization not working")
	}
	pos = 11
	move = -1
	res = getNormalizedPosition(pos, move)
	if res < 0 || res > 11 {
		t.Error("normalization not working")
	}
	pos = 8
	move = 2
	res = getNormalizedPosition(pos, move)
	if res < 0 || res > 11 {
		t.Error("normalization not working")
	}
	pos = 10
	move = 2
	res = getNormalizedPosition(pos, move)
	if res < 0 || res > 11 {
		t.Error("normalization not working")
	}
}

func TestGetEnharmonicKey(t *testing.T) {
	res := getEnharmonicKey(`Cb`)
	if res == `Cb` {
		t.Error("key not enharmonic changed")
	}
	res = getEnharmonicKey(`Fb`)
	if res == `Fb` {
		t.Error("key not enharmonic changed")
	}
	res = getEnharmonicKey(`B#`)
	if res == `B#` {
		t.Error("key not enharmonic changed")
	}
	res = getEnharmonicKey(`F##`)
	if res == `F##` {
		t.Error("key not enharmonic changed")
	}
	res = getEnharmonicKey(`E#`)
	if res == `E#` {
		t.Error("key not enharmonic changed")
	}
	res = getEnharmonicKey(`A#`)
	if res == `A#` {
		t.Error("key not enharmonic changed")
	}
	res = getEnharmonicKey(`C##`)
	if res == `C##` {
		t.Error("key not enharmonic changed")
	}
	res = getEnharmonicKey(`D#`)
	if res == `D#` {
		t.Error("key not enharmonic changed")
	}
}

func TestCoF_ChurchModeKeys(t *testing.T) {
	var testrow = struct {
		key  string
		name string
		mode Mode
		nbr  int
		par  string
		ekey string
		erel string
		enbr int
		epar string
	}{
		``,
		``,
		Ionian,
		0,
		``,
		``,
		``,
		0,
		``,
	}
	var testtable = []struct {
		key  string
		name string
		mode Mode
		nbr  int
		par  string
		ekey string
		erel string
		enbr int
		epar string
	}{}
	c := HarmonyModels.CreateCoF(true)
	for k := 0; k < 12; k++ {
		for i := 2; i < 8; i++ {
			mode := Mode(i)
			ckey := HarmonyModels.createKey(c.keys[k].KeyNotes[strconv.Itoa(i)], mode)
			testrow.key = c.keys[k].Name
			testrow.name = c.keys[k].KeyNotes[strconv.Itoa(i)]
			testrow.mode = mode
			testrow.nbr = len(c.keys[k].Signs)
			testrow.par = HarmonyModels.createKey(getEnharmonicKey(ckey.Name), Ionian).Name
			testrow.ekey = ckey.Name
			testrow.erel = ckey.Relative().Name
			testrow.enbr = len(ckey.Signs)
			testrow.epar = getEnharmonicKey(ckey.Parallel().Name)
			testtable = append(testtable, testrow)
		}
	}
	for _, tr := range testtable {
		if tr.key != tr.erel {
			t.Errorf("expected %s as relative but got %s", tr.key, tr.erel)
		}
		if tr.name != tr.ekey {
			t.Errorf("expected %s as name but got %s", tr.name, tr.ekey)
		}
		if tr.nbr != tr.enbr {
			t.Errorf("expected %d as nbr of signs but got %d", tr.nbr, tr.enbr)
		}
		if tr.epar != getEnharmonicKey(tr.name) {
			t.Errorf("expected %s as parallel but got %s", getEnharmonicKey(tr.name), tr.epar)
		}
		if tr.epar != tr.par {
			t.Errorf("expected %s as parallel but got %s", tr.par, tr.epar)
		}
	}
}

func ExampleScale_FromKey() {
	s := Scale{
		Name:        `Test`,
		DisplayName: `Test`,
		ScaleType:   `Heptatonic`,
		Formula:     scaleFormulas[`lydian`],
	}
	notes := s.FromKey(HarmonyModels.CreateCoF(false).ChurchModeKeys(Lydian)[0])
	fmt.Println(notes.ToString())
	notes = s.FromKey(HarmonyModels.CreateCoF(false).ChurchModeKeys(Lydian)[6])
	fmt.Println(notes.ToString())
	notes = s.FromKey(HarmonyModels.CreateCoF(false).ChurchModeKeys(Ionian)[0])
	fmt.Println(notes.ToString())
	s.Formula = scaleFormulas[`dorian`]
	notes = s.FromKey(HarmonyModels.CreateCoF(false).ChurchModeKeys(Ionian)[0])
	fmt.Println(notes.ToString())
	notes = s.FromKey(HarmonyModels.CreateCoF(false).ChurchModeKeys(Ionian)[9])
	fmt.Println(notes.ToString())
	// Output:
	// [F G A B C D E]
	// [B C♯ D♯ E♯ F♯ G♯ A♯]
	// [C D E F♯ G A B]
	// [C D E♭ F G A B♭]
	// [E♭ F G♭ A♭ B♭ C D♭]
}

func TestScale_FromKey(t *testing.T) {
	s := Scale{
		Name:        `Test`,
		DisplayName: `Test`,
		ScaleType:   `Heptatonic`,
		Formula:     scaleFormulas[`melodic`],
	}
	k := HarmonyModels.createKey(`C`, Ionian)
	notes := s.FromKey(k)
	if notes.ToString()[2] != `E♭` || notes.ToString()[5] != `A` {
		t.Error("melodic minor in C not correct ", notes.ToString())
	}
	k = HarmonyModels.createKey(`A`, Aeolian)
	notes = s.FromKey(k)
	if notes.ToString()[2] != `C` || notes.ToString()[6] != `G♯` {
		t.Error("melodic minor in C not correct ", notes.ToString())
	}
	k = HarmonyModels.createKey(`F`, Ionian)
	notes = s.FromKey(k)
	if notes.ToString()[2] != `A♭` || notes.ToString()[5] != `D` {
		t.Error("melodic minor in C not correct ", notes.ToString())
	}
	k = HarmonyModels.createKey(`F#`, Ionian)
	notes = s.FromKey(k)
	if notes.ToString()[2] != `A` || notes.ToString()[6] != `E♯` {
		t.Error("melodic minor in C not correct ", notes.ToString())
	}
}

func TestModelsCreateKeyScale(t *testing.T) {
	cof := HarmonyModels.CreateCoF(false)
	keys := cof.MajorKeys()
	for i := range keys {
		ks := HarmonyModels.createKeyScale(keys[i], `dorian`)
		if ks.Tonality != Minor || ks.ScaleType != scaleTypes[7] {
			t.Errorf("expected minor and heptatonic but got different %s, %v", ks.Tonality, ks.ScaleType)
		}
	}
	for i := range keys {
		ks := HarmonyModels.createKeyScale(keys[i], `ionian`)
		if ks.Tonality != Major || ks.ScaleType != scaleTypes[7] {
			t.Errorf("expected major and heptatonic but got different %s, %v", ks.Tonality, ks.ScaleType)
		}
	}
	none := getThird([]string{})
	ks := HarmonyModels.createKeyScale(keys[0], `chromatic ascending`)
	if ks.Tonality != none || ks.ScaleType != scaleTypes[12] {
		t.Errorf("expeted chroamtic but got %s %v", ks.Tonality, ks.ScaleType)
	}
	if ks.FullName != `C-Chromatic Ascending` {
		t.Errorf("expected C-Chromatic Ascending but got %s", ks.FullName)
	}

	if len(ks.Notes) != 12 || ks.Notes[1].Name != `C#` {
		t.Errorf("expeted chroamtic asc of C but got %s %d", ks.Notes[1].Name, len(ks.Notes))
	}
}
