package harmony

// INoter interface provides the function FromKey which allows to return a NoteList of a key
type INoter interface {
	FromKey(Key) NoteList
}

// ITonality provieds the Tonality function which returns the tonlity of the underlying type (interval, chord, scale)
type ITonality interface {
	Tonality() Tonality
}

// ICoF descibes the functionality of a circle of fifths (getting ring and segment keys)
type ICoF interface {
	MajorKeys() KeyList
	MinorKeys() KeyList
	ChurchModeKeys(Mode) KeyList
	Segment(int) KeyList
}

// IKeyList defines function to return an specific element of a key list
type IKeyList interface {
	ByName(string) Key
	ByPosition(int) Key
}
