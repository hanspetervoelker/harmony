package harmony

const (
	accidentalNaturalSymbol     string = `♮`
	accidentalSharpSymbol       string = `♯`
	accidentalFlatSymbol        string = `♭`
	accidentalDoubleSharpSymbol string = `𝄪`
	accidentalDoubleFlatSymbol  string = `𝄫`
	accidentalNatural           string = ``
	accidentalSharp             string = `#`
	accidentalFlat              string = `b`
	accidentalDoubleSharp       string = `##`
	accidentalDoubleFlat        string = `bb`
	alterDiminished             string = `diminished`
	alterAugmented              string = `augmented`
	noAlteration                string = ``
)

// Tonality defines the tonality of a chord or scale (identified by third).
// Possible values are major, minor, none
type Tonality string

const (
	// Major means major third
	Major Tonality = `major`
	// Minor means minor third
	Minor Tonality = `minor`
	// None means that nor third was found
	None Tonality = `none`
)

// TriadType indicate one of four possible triad types (major, minor, augmented, diminished)
type TriadType string

const (
	// MajorTriad = major third and perfect fifth
	MajorTriad TriadType = `major`
	// MinorTriad = minor third and perfect fifth
	MinorTriad TriadType = `minor`
	// AugmentedTriad = major third and augmented fifth
	AugmentedTriad TriadType = `augmented`
	// DiminishedTriad = minor third and diminished fifth
	DiminishedTriad TriadType = `diminished`
	// NoneTriad means that triad could not be evaluated due to missing third or fifth
	NoneTriad TriadType = `none`
)

// Mode defines a churchmode Ionian - Locrian
type Mode int

const (
	// Ionian = first mode = major
	Ionian Mode = iota + 1
	// Dorian = second mode
	Dorian
	// Phygrian - third mode
	Phygrian
	// Lydian = fourth mode
	Lydian
	// Mixolydian = fifth mode = dominant
	Mixolydian
	// Aeolian = sixth mode = minor
	Aeolian
	// Locrian = seventh mode
	Locrian
)
