package harmony

// Key defines the attributes for a specific key
type Key struct {
	Name           string            `json:"name"`
	DisplayName    string            `json:"displayName"`
	Signs          []string          `json:"signs"`
	KeyNotes       map[string]string `json:"notes"`
	CirclePosition int               `json:"position"`
	Mode           Mode              `json:"mode"`
	relative       string
	parallel       string
	left           string
	right          string
}

// KeyList is a list of (CoF-) Keys
type KeyList []Key

// Note describes a specific note
type Note struct {
	Name        string `json:"name"`
	DisplayName string `json:"displayName"`
	Accidental  string `json:"accidental"`
	Natural     bool   `json:"natural"`
	Enharmonic  bool   `json:"enharmonic"`
}

// NoteList is a slice of Note and lists a specific set of notes
type NoteList []Note

// Interval describes the attributes of an musical interval
type Interval struct {
	Name        string `json:"name"`
	DisplayName string `json:"displayName"`
	ShortName   string `json:"shortName"`
	FormulaName string `json:"formulaName"`
	Index       int    `json:"index"`
	Alteration  string `json:"alteration"`
}

// Chord describes an chord
type Chord struct {
	Name              string   `json:"name"`
	DisplayName       string   `json:"displayName"`
	ChordType         string   `json:"type"`
	ChordNotesNbrType string   `json:"countType"`
	ChordIntervalType string   `json:"intervalType"`
	TriadType         string   `json:"triadType"`
	Tonality          Tonality `json:"tonality"`
	Formula           []string `json:"formula"`
}

// Scale describes a musical scale
type Scale struct {
	Name        string   `json:"name"`
	DisplayName string   `json:"displayName"`
	ScaleType   string   `json:"type"`
	Tonality    Tonality `json:"tonality"`
	Formula     []string `json:"formula"`
}

// KeyScale is a scale for a specific key (notes) and extends Scale
type KeyScale struct {
	FullName string
	Signs    []string
	Notes    NoteList
	Scale
}

// Models is the overall Model of this package (empty type) and provides functions to access models
type Models struct{}

// CoF represents a circle of fifths
type CoF struct {
	keys map[int]Key
}
