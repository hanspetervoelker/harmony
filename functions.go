package harmony

import (
	"strconv"
	"strings"
)

// Distance calculates the distance (absolute value) between the current interval and a given interval
func (iv Interval) Distance(other Interval) int {
	dist := other.Index - iv.Index
	if dist < 0 {
		dist = dist * -1
	}
	return dist
}

// FromKey is the implementation of INoter interface and returns the note(as notelist) from a key for that interval
func (iv Interval) FromKey(k Key) NoteList {
	n := k.KeyNotes[iv.FormulaName]
	return []Note{
		notes[n],
	}
}

// FromKey s the implementation of INoter interface and returns the notes corresponding to the scale formula. The formula base is always a major key.
func (s Scale) FromKey(k Key) NoteList {
	nl := NoteList{}
	n := s.Formula
	for _, interval := range n {
		nl = append(nl, notes[k.KeyNotes[interval]])
	}
	return nl
}

// Tonality is the implementation of the ITonality interface and determines the tonality (major, minor, none) of an interval.Tonality
// Returns minor for b3, major for 3 and none for all others
func (iv Interval) Tonality() Tonality {
	if iv.FormulaName == `b3` {
		return Minor
	}
	if iv.FormulaName == `3` {
		return Major
	}
	return None
}

// ToString trasforms a NoteList to a slice of string (note names)
func (nl NoteList) ToString() []string {
	res := make([]string, len(nl))
	for i := range nl {
		res[i] = nl[i].DisplayName
	}
	return res
}

func (m Models) createKey(name string, mode Mode) Key {
	var rel, l, r string

	//name = getEnharmonicKey(name)
	var s = signs[name]
	var cp = cof[name]
	var kn = keyNotes[name]
	if mode != Ionian {
		rel = keyNotes[name][relIntervals[mode]]
		l = keyNotes[rel][`4`]
		r = keyNotes[rel][`5`]
		s = signs[getEnharmonicKey(rel)]
		cp = cof[getEnharmonicKey(rel)]

	} else {
		rel = keyNotes[name][`6`]
		l = keyNotes[name][`4`]
		r = keyNotes[name][`5`]
	}
	return Key{
		Name:           name,
		DisplayName:    prettyPrintName(name),
		Signs:          s,
		KeyNotes:       kn,
		CirclePosition: cp,
		Mode:           mode,
		relative:       getEnharmonicKey(rel),
		parallel:       getEnharmonicKey(name),
		left:           getEnharmonicKey(l),
		right:          getEnharmonicKey(r),
	}
}

// Left returns the next key counterclockwise in the circle of fifths
func (k Key) Left() Key {
	return HarmonyModels.createKey(k.left, Ionian)
}

// Right returns the next clockwise key in the circle of fifths
func (k Key) Right() Key {
	return HarmonyModels.createKey(k.right, Ionian)
}

// Relative returns the relative major (if mode is not major) or relative minor key (if mode is major)
func (k Key) Relative() Key {
	var m = Aeolian
	if k.Mode != Ionian {
		m = Ionian
	}
	return HarmonyModels.createKey(k.relative, m)
}

// Parallel returns the parallel major (if mode!=ionian) or the parallel minor (if mode=ionian)
func (k Key) Parallel() Key {
	var m = Aeolian
	if k.Mode != Ionian {
		m = Ionian
	}
	return HarmonyModels.createKey(k.parallel, m)
}

// ByName is the implementation of the IKeyList interface and returns a key of a keylist found by it's name
func (kl KeyList) ByName(name string) Key {
	for _, v := range kl {
		if v.Name == name {
			return v
		}
	}
	return Key{}
}

// ByPosition is the implementation of the IKeyList interface and returns a key of a keylist found by it's circle position
func (kl KeyList) ByPosition(pos int) Key {
	for _, v := range kl {
		if v.CirclePosition == pos {
			return v
		}
	}
	return Key{}
}

// CreateCoF creates a circle of fiths. UseGb indicates if Gb or F# is used on the sixth position of the circle
func (m Models) CreateCoF(useGb bool) CoF {
	var six Key
	if useGb {
		six = m.createKey(`Gb`, Ionian)
	} else {
		six = m.createKey(`F#`, Ionian)
	}
	return CoF{
		keys: map[int]Key{
			0:  m.createKey(`C`, Ionian),
			1:  m.createKey(`G`, Ionian),
			2:  m.createKey(`D`, Ionian),
			3:  m.createKey(`A`, Ionian),
			4:  m.createKey(`E`, Ionian),
			5:  m.createKey(`B`, Ionian),
			6:  six,
			7:  m.createKey(`Db`, Ionian),
			8:  m.createKey(`Ab`, Ionian),
			9:  m.createKey(`Eb`, Ionian),
			10: m.createKey(`Bb`, Ionian),
			11: m.createKey(`F`, Ionian),
		},
	}
}

// MajorKeys is an implementation of ICoF and returns all 12 major keys of that circle
func (c CoF) MajorKeys() KeyList {
	var res = make(KeyList, 12)
	for k := range c.keys {
		res[k] = c.keys[k]
	}
	return res
}

// MinorKeys is an implementation of iCoF and returns all 12 minor keys of that circle
func (c CoF) MinorKeys() KeyList {
	var res = make(KeyList, 12)
	for k := range c.keys {
		res[k] = c.keys[k].Relative()
	}
	return res
}

// ChurchModeKeys returns all 12 keys of a specific mode
func (c CoF) ChurchModeKeys(mode Mode) KeyList {
	var res = make(KeyList, 12)
	for k := range c.keys {
		keynotes := c.keys[k].KeyNotes
		name := keynotes[strconv.Itoa(int(mode))]
		//fmt.Println(name)
		res[k] = HarmonyModels.createKey(name, mode)
	}
	return res
}

// Segment returns all churchmodes for a specific cof position (segment)
func (c CoF) Segment(pos int) KeyList {
	var res = make(KeyList, 7)
	kn := c.keys[pos].KeyNotes
	for i := 1; i < 8; i++ {
		res[i-1] = HarmonyModels.createKey(kn[strconv.Itoa(i)], Mode(i))
	}
	return res
}

func (mo Mode) String() string {
	switch mo {
	case Ionian:
		return `Ionian`
	case Dorian:
		return `Dorian`
	case Phygrian:
		return `Phygrian`
	case Lydian:
		return `Lydian`
	case Mixolydian:
		return `Mixolydian`
	case Aeolian:
		return `Aeolian`
	case Locrian:
		return `Locrian`
	default:
		return strconv.Itoa(int(mo))
	}
}

func (m Models) createKeyScale(k Key, scaleName string) KeyScale {
	var res = KeyScale{}
	tonality := None
	var scale = Scale{}
	f := scaleFormulas[scaleName]
	scaleType := scaleTypes[len(f)]
	if len(f) < 8 && len(f) > 4 {
		tonality = getThird(f)
	}
	scale.Formula = f
	scale.Name = scaleName
	scale.DisplayName = strings.Title(scaleName)
	scale.ScaleType = scaleType
	scale.Tonality = tonality
	res.FullName = k.DisplayName + `-` + scale.DisplayName
	res.Signs = k.Signs
	res.Notes = scale.FromKey(k)
	res.Scale = scale
	return res
}
