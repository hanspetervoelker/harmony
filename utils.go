package harmony

import "strings"

func prettyPrintName(name string) string {
	return strings.Replace(strings.Replace(name, accidentalSharp, accidentalSharpSymbol, 1), accidentalFlat, accidentalFlatSymbol, 1)
}

func getEnharmonicKey(name string) string {
	switch name {
	case `Cb`:
		return `B`
	case `Fb`:
		return `E`
	case `B#`:
		return `C`
	case `F##`:
		return `G`
	case `C##`:
		return `D`
	case `C#`:
		return `Db`
	case `G#`:
		return `Ab`
	case `A#`:
		return `Bb`
	case `E#`:
		return `F`
	case `D#`:
		return `Eb`
	default:
		return name
	}
}

func getNormalizedPosition(pos int, move int) int {
	if pos+move > 11 {
		return pos + move - 12
	}
	if pos+move < 0 {
		return pos + move + 12
	}
	return pos + move
}

/*
func shiftIntervals(keynotes map[string]string, interval string) map[string]string {
	var res = make(map[string]string, len(keyNoteIntervals))
	for _, v := range keyNoteIntervals {

	}
	return res
}
*/

func getThird(f []string) Tonality {
	for _, v := range f {
		if v == `b3` {
			return Minor
		}
		if v == `3` {
			return Major
		}
	}
	return None
}
