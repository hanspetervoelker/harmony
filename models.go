package harmony

var keyNotes = map[string]map[string]string{
	`C`:  {`1`: `C`, `#1`: `C#`, `b2`: `Db`, `2`: `D`, `#2`: `D#`, `b3`: `Eb`, `3`: `E`, `4`: `F`, `#4`: `F#`, `b5`: `Gb`, `5`: `G`, `#5`: `G#`, `b6`: `Ab`, `6`: `A`, `#6`: `A#`, `b7`: `Bb`, `7`: `B`},
	`G`:  {`1`: `G`, `#1`: `G#`, `b2`: `Ab`, `2`: `A`, `#2`: `A#`, `b3`: `Bb`, `3`: `B`, `4`: `C`, `#4`: `C#`, `b5`: `Db`, `5`: `D`, `#5`: `D#`, `b6`: `Eb`, `6`: `E`, `#6`: `E#`, `b7`: `F`, `7`: `F#`},
	`D`:  {`1`: `D`, `#1`: `D#`, `b2`: `Eb`, `2`: `E`, `#2`: `E#`, `b3`: `F`, `3`: `F#`, `4`: `G`, `#4`: `G#`, `b5`: `Ab`, `5`: `A`, `#5`: `A#`, `b6`: `Bb`, `6`: `B`, `#6`: `B#`, `b7`: `C`, `7`: `C#`},
	`A`:  {`1`: `A`, `#1`: `A#`, `b2`: `Bb`, `2`: `B`, `#2`: `B#`, `b3`: `C`, `3`: `C#`, `4`: `D`, `#4`: `D#`, `b5`: `Eb`, `5`: `E`, `#5`: `E#`, `b6`: `F`, `6`: `F#`, `#6`: `F##`, `b7`: `G`, `7`: `G#`},
	`E`:  {`1`: `E`, `#1`: `E#`, `b2`: `F`, `2`: `F#`, `#2`: `F##`, `b3`: `G`, `3`: `G#`, `4`: `A`, `#4`: `A#`, `b5`: `Bb`, `5`: `B`, `#5`: `B#`, `b6`: `C`, `6`: `C#`, `#6`: `C##`, `b7`: `D`, `7`: `D#`},
	`B`:  {`1`: `B`, `#1`: `B#`, `b2`: `C`, `2`: `C#`, `#2`: `C##`, `b3`: `D`, `3`: `D#`, `4`: `E`, `#4`: `E#`, `b5`: `F`, `5`: `F#`, `#5`: `F##`, `b6`: `G`, `6`: `G#`, `#6`: `G##`, `b7`: `A`, `7`: `A#`},
	`F#`: {`1`: `F#`, `#1`: `F##`, `b2`: `G`, `2`: `G#`, `#2`: `G##`, `b3`: `A`, `3`: `A#`, `4`: `B`, `#4`: `B#`, `b5`: `C`, `5`: `C#`, `#5`: `C##`, `b6`: `D`, `6`: `D#`, `#6`: `D##`, `b7`: `E`, `7`: `E#`},
	`C#`: {`1`: `C#`, `#1`: `C##`, `b2`: `D`, `2`: `D#`, `#2`: `D##`, `b3`: `E`, `3`: `E#`, `4`: `F#`, `#4`: `F##`, `b5`: `G`, `5`: `G#`, `#5`: `G##`, `b6`: `A`, `6`: `A#`, `#6`: `A##`, `b7`: `B`, `7`: `B#`},
	`G#`: {`1`: `G#`, `#1`: `G##`, `b2`: `A`, `2`: `A#`, `#2`: `A##`, `b3`: `B`, `3`: `B#`, `4`: `C#`, `#4`: `C##`, `b5`: `D`, `5`: `D#`, `#5`: `D##`, `b6`: `E`, `6`: `E#`, `#6`: `E##`, `b7`: `F#`, `7`: `F##`},
	`D#`: {`1`: `D#`, `#1`: `D##`, `b2`: `E`, `2`: `E#`, `#2`: `E##`, `b3`: `F#`, `3`: `F##`, `4`: `G#`, `#4`: `G##`, `b5`: `A`, `5`: `A#`, `#5`: `A##`, `b6`: `B`, `6`: `B#`, `#6`: `B##`, `b7`: `C#`, `7`: `C##`},
	`A#`: {`1`: `A#`, `#1`: `A##`, `b2`: `B`, `2`: `B#`, `#2`: `B##`, `b3`: `C#`, `3`: `C##`, `4`: `D#`, `#4`: `D##`, `b5`: `E`, `5`: `E#`, `#5`: `E##`, `b6`: `F#`, `6`: `F##`, `#6`: `G#`, `b7`: `G#`, `7`: `G##`},
	`Cb`: {`1`: `Cb`, `#1`: `C`, `b2`: `Dbb`, `2`: `Db`, `#2`: `D`, `b3`: `Ebb`, `3`: `Eb`, `4`: `Fb`, `#4`: `F`, `b5`: `Gbb`, `5`: `Gb`, `#5`: `G`, `b6`: `Abb`, `6`: `Ab`, `#6`: `A`, `b7`: `Bb`, `7`: `B`},
	`Gb`: {`1`: `Gb`, `#1`: `G`, `b2`: `Abb`, `2`: `Ab`, `#2`: `A`, `b3`: `Bbb`, `3`: `Bb`, `4`: `Cb`, `#4`: `C`, `b5`: `Dbb`, `5`: `Db`, `#5`: `D`, `b6`: `Ebb`, `6`: `Eb`, `#6`: `E`, `b7`: `Fb`, `7`: `F`},
	`Db`: {`1`: `Db`, `#1`: `D`, `b2`: `Ebb`, `2`: `Eb`, `#2`: `E`, `b3`: `Fb`, `3`: `F`, `4`: `Gb`, `#4`: `G`, `b5`: `Abb`, `5`: `Ab`, `#5`: `A`, `b6`: `Bbb`, `6`: `Bb`, `#6`: `B`, `b7`: `Cb`, `7`: `C`},
	`Ab`: {`1`: `Ab`, `#1`: `A`, `b2`: `Bbb`, `2`: `Bb`, `#2`: `B`, `b3`: `Cb`, `3`: `C`, `4`: `Db`, `#4`: `D`, `b5`: `Ebb`, `5`: `Eb`, `#5`: `E`, `b6`: `Fb`, `6`: `F`, `#6`: `F#`, `b7`: `Gb`, `7`: `G`},
	`Eb`: {`1`: `Eb`, `#1`: `E`, `b2`: `Fbb`, `2`: `F`, `#2`: `F#`, `b3`: `Gb`, `3`: `G`, `4`: `Ab`, `#4`: `A`, `b5`: `Bbb`, `5`: `Bb`, `#5`: `B`, `b6`: `Cb`, `6`: `C`, `#6`: `C#`, `b7`: `Db`, `7`: `D`},
	`Bb`: {`1`: `Bb`, `#1`: `B`, `b2`: `Cb`, `2`: `C`, `#2`: `C#`, `b3`: `Db`, `3`: `D`, `4`: `Eb`, `#4`: `E`, `b5`: `Fbb`, `5`: `F`, `#5`: `F#`, `b6`: `Gb`, `6`: `G`, `#6`: `G#`, `b7`: `Ab`, `7`: `A`},
	`F`:  {`1`: `F`, `#1`: `F#`, `b2`: `Gb`, `2`: `G`, `#2`: `G#`, `b3`: `Ab`, `3`: `A`, `4`: `Bb`, `#4`: `B`, `b5`: `Cb`, `5`: `C`, `#5`: `C#`, `b6`: `Db`, `6`: `D`, `#6`: `D#`, `b7`: `Eb`, `7`: `E`},
	`E#`: {`1`: `E#`, `#1`: `E##`, `b2`: `F#`, `2`: `F##`, `#2`: `G#`, `b3`: `G#`, `3`: `G##`, `4`: `A#`, `#4`: `A##`, `b5`: `B`, `5`: `B#`, `#5`: `B##`, `b6`: `C#`, `6`: `C##`, `#6`: `D#`, `b7`: `D#`, `7`: `D##`},
}

var keyNoteIntervals = []string{`1`, `1#`, `b2`, `2`, `#2`, `b3`, `3`, `4`, `#4`, `b5`, `5`, `#5`, `b6`, `6`, `b7`, `7`}

var signs = map[string][]string{
	`C`:  {},
	`G`:  {`F#`},
	`D`:  {`F#`, `C#`},
	`A`:  {`F#`, `C#`, `G#`},
	`E`:  {`F#`, `C#`, `G#`, `D#`},
	`B`:  {`F#`, `C#`, `G#`, `D#`, `A#`},
	`F#`: {`F#`, `C#`, `G#`, `D#`, `A#`, `E#`},
	`C#`: {`F#`, `C#`, `G#`, `D#`, `A#`, `E#`, `B#`},
	`G#`: {`F#`, `C#`, `G#`, `D#`, `A#`, `E#`, `B#`, `F##`},
	`D#`: {`F#`, `C#`, `G#`, `D#`, `A#`, `E#`, `B#`, `F##`, `C##`},
	`Cb`: {`Bb`, `Eb`, `Ab`, `Db`, `Gb`, `Cb`, `Fb`},
	`Gb`: {`Bb`, `Eb`, `Ab`, `Db`, `Gb`, `Cb`},
	`Db`: {`Bb`, `Eb`, `Ab`, `Db`, `Gb`},
	`Ab`: {`Bb`, `Eb`, `Ab`, `Db`},
	`Eb`: {`Bb`, `Eb`, `Ab`},
	`Bb`: {`Bb`, `Eb`},
	`F`:  {`Bb`},
}

var degrees = map[int]string{
	1: `tonic`,
	2: `supertonic`,
	3: `mediant`,
	4: `subdominant`,
	5: `dominant`,
	6: `subdmediant`,
	7: `subtonic`,
}

var notes = map[string]Note{
	`C`:   {`C`, `C`, accidentalNatural, true, false},
	`Dbb`: {`Dbb`, `D𝄫`, accidentalDoubleFlat, false, true},
	`B#`:  {`B#`, `B♯`, accidentalSharp, false, true},
	`C#`:  {`C#`, `C♯`, accidentalSharp, false, false},
	`Db`:  {`Db`, `D♭`, accidentalFlat, false, false},
	`B##`: {`B##`, `B𝄪`, accidentalDoubleSharp, false, true},
	`D`:   {`D`, `D`, accidentalNatural, true, false},
	`C##`: {`C##`, `C𝄪`, accidentalDoubleSharp, false, true},
	`D#`:  {`D#`, `D♯`, accidentalSharp, false, false},
	`Eb`:  {`Eb`, `E♭`, accidentalFlat, false, false},
	`Fbb`: {`Fbb`, `F𝄫`, accidentalDoubleFlat, false, true},
	`E`:   {`E`, `E`, accidentalNatural, true, false},
	`D##`: {`D##`, `D𝄪`, accidentalDoubleSharp, false, true},
	`Ebb`: {`Ebb`, `E𝄫`, accidentalDoubleFlat, false, true},
	`Fb`:  {`Fb`, `F♭`, accidentalFlat, false, true},
	`F`:   {`F`, `F`, accidentalNatural, true, false},
	`E#`:  {`E#`, `E♯`, accidentalSharp, false, true},
	`Gbb`: {`Gbb`, `G𝄫`, accidentalDoubleFlat, false, true},
	`F#`:  {`F#`, `F♯`, accidentalSharp, false, false},
	`Gb`:  {`Gb`, `G♭`, accidentalFlat, false, false},
	`E##`: {`E##`, `E𝄪`, accidentalDoubleSharp, false, true},
	`G`:   {`G`, `G`, accidentalNatural, true, false},
	`Abb`: {`Abb`, `A𝄫`, accidentalDoubleFlat, false, true},
	`F##`: {`F##`, `F𝄪`, accidentalDoubleSharp, false, true},
	`G#`:  {`G#`, `G♯`, accidentalSharp, false, false},
	`Ab`:  {`Ab`, `A♭`, accidentalFlat, false, false},
	`A`:   {`A`, `A`, accidentalNatural, true, false},
	`G##`: {`G##`, `G𝄪`, accidentalDoubleSharp, false, true},
	`Bbb`: {`Bbb`, `B𝄫`, accidentalDoubleFlat, false, true},
	`A#`:  {`A#`, `A♯`, accidentalSharp, false, false},
	`Bb`:  {`Bb`, `B♭`, accidentalFlat, false, false},
	`Cbb`: {`Cbb`, `C𝄫`, accidentalDoubleFlat, false, true},
	`B`:   {`B`, `B`, accidentalNatural, true, false},
	`Cb`:  {`Cb`, `C♭`, accidentalFlat, false, true},
	`A##`: {`A##`, `A𝄪`, accidentalDoubleSharp, false, true},
}

var intervals = map[string]Interval{
	`1`:   {`perfect unison`, `P1`, `P1`, `1`, 0, noAlteration},
	`bb2`: {`diminished second`, `d2`, `d2`, `bb2`, 0, alterDiminished},
	`b2`:  {`minor second`, `m2`, `b2`, `b2`, 1, noAlteration},
	`#1`:  {`augmented unison`, `A1`, `A1`, `#1`, 1, alterAugmented},
	`2`:   {`major second`, `M2`, `M2`, `2`, 2, noAlteration},
	`bb3`: {`diminshed third`, `d3`, `d3`, `bb3`, 2, alterDiminished},
	`b3`:  {`minor third`, `m3`, `m3`, `b3`, 3, noAlteration},
	`#2`:  {`augmented second`, `A2`, `A2`, `#2`, 3, alterAugmented},
	`3`:   {`major third`, `M3`, `M3`, `3`, 4, noAlteration},
	`b4`:  {`diminished fourth`, `d4`, `d4`, `b4`, 4, alterDiminished},
	`4`:   {`perfect fourth`, `P4`, `P4`, `4`, 5, noAlteration},
	`#3`:  {`augmented third`, `A3`, `A3`, `#3`, 5, alterAugmented},
	`b5`:  {`diminshed fifth`, `d5`, `d5`, `b5`, 6, alterDiminished},
	`#4`:  {`augmented fourth`, `A4`, `A4`, `#4`, 6, alterAugmented},
	`5`:   {`perfect fifth`, `P5`, `P5`, `5`, 7, noAlteration},
	`bb6`: {`diminished sixth`, `d6`, `d6`, `bb6`, 7, alterDiminished},
	`b6`:  {`minor sixth`, `m6`, `m6`, `b6`, 8, noAlteration},
	`#5`:  {`augmented fifth`, `A5`, `A5`, `#5`, 8, alterAugmented},
	`6`:   {`major sixth`, `M6`, `M6`, `6`, 9, noAlteration},
	`bb7`: {`diminished seventh`, `d7`, `d7`, `bb7`, 9, alterDiminished},
	`b7`:  {`minor seventh`, `m7`, `m7`, `b7`, 10, noAlteration},
	`#6`:  {`augmented sixth`, `A6`, `A6`, `#6`, 10, alterAugmented},
	`7`:   {`major seventh`, `M7`, `M7`, `7`, 11, noAlteration},
	`b8`:  {`diminished octave`, `d8`, `d8`, `b8`, 11, alterDiminished},
	`8`:   {`perfect octave`, `P8`, `P8`, `8`, 12, noAlteration},
	`#7`:  {`augmented seventh`, `A7`, `A7`, `#7`, 12, alterAugmented},
}

var scaleTypes = map[int]string{
	12: `Dodecatonic`,
	8:  `Octatonic`,
	7:  `Heptatonic`,
	6:  `Hexatonic`,
	5:  `Pentatonic`,
}

var chordTypes = map[string]string{
	`seventh`:   `seventh`,
	`triad`:     `triad`,
	`added`:     `added`,
	`suspended`: `suspended`,
	`extended`:  `extended`,
	`altered`:   `altered`,
}

var chordNotesNbr = map[int]string{
	2: `Dyad`,
	3: `Triad`,
	4: `Tetrad`,
	5: `Pentad`,
	6: `Hexad`,
	7: `Heptad`,
}

var chordIntervals = map[int]string{
	2: `secundal`,
	3: `tertian`,
	4: `quartal`,
	5: `quintal`,
}

var modes = map[Mode]string{
	Ionian:     `ionian`,
	Dorian:     `dorian`,
	Phygrian:   `phygrian`,
	Lydian:     `lydian`,
	Mixolydian: `mixolydian`,
	Aeolian:    `aeolian`,
	Locrian:    `locrian`,
}

var scaleFormulas = map[string][]string{
	`ionian`:               {`1`, `2`, `3`, `4`, `5`, `6`, `7`},
	`dorian`:               {`1`, `2`, `b3`, `4`, `5`, `6`, `b7`},
	`phygrian`:             {`1`, `b2`, `b3`, `4`, `5`, `b6`, `b7`},
	`lydian`:               {`1`, `2`, `3`, `#4`, `5`, `6`, `7`},
	`mixolydian`:           {`1`, `2`, `3`, `4`, `5`, `6`, `b7`},
	`aeolian`:              {`1`, `2`, `b3`, `4`, `5`, `b6`, `b7`},
	`locrian`:              {`1`, `b2`, `b3`, `4`, `b5`, `b6`, `b7`},
	`harmonic`:             {`1`, `2`, `b3`, `4`, `5`, `b6`, `7`},
	`melodic`:              {`1`, `2`, `b3`, `4`, `5`, `6`, `7`},
	`phygrian dominant`:    {`1`, `b2`, `3`, `4`, `5`, `b6`, `b7`},
	`chromatic ascending`:  {`1`, `#1`, `2`, `#2`, `3`, `4`, `#4`, `5`, `#5`, `6`, `#6`, `7`},
	`chromatic descending`: {`1`, `b2`, `2`, `b3`, `3`, `4`, `b5`, `5`, `b6`, `6`, `b7`, `7`},
}

var chordFormulas = map[string][]string{
	`majorTriad`:              {`1`, `3`, `5`},
	`minorTriad`:              {`1`, `b3`, `5`},
	`augmentedTriad`:          {`1`, `3`, `#5`},
	`diminishedTriad`:         {`1`, `b3`, `b5`},
	`diminishedSeventh`:       {`1`, `b3`, `b5`, `bb7`},
	`halfDiminishedSeventh`:   {`1`, `b3`, `b5`, `b7`},
	`minorSeventh`:            {`1`, `b3`, `5`, `b7`},
	`minorMajorSeventh`:       {`1`, `b3`, `5`, `7`},
	`dominantSeventh`:         {`1`, `3`, `5`, `b7`},
	`majorSeventh`:            {`1`, `3`, `5`, `7`},
	`augmentedSeventh`:        {`1`, `3`, `#5`, `b7`},
	`augmentedMajorSeventh`:   {`1`, `3`, `#5`, `7`},
	`dominantNinth`:           {`1`, `3`, `5`, `b7`, `2`},
	`dominantEleven`:          {`1`, `3`, `5`, `b7`, `2`, `4`},
	`dominantThirdteen`:       {`1`, `3`, `5`, `b7`, `2`, `4`, `6`},
	`seventhsFlatNinth`:       {`1`, `3`, `5`, `b7`, `b2`},
	`seventhsSharpNinth`:      {`1`, `3`, `5`, `b7`, `#2`},
	`seventhsSharpEleventh`:   {`1`, `3`, `5`, `b7`, `#4`},
	`seventhsFlatEleventh`:    {`1`, `3`, `5`, `b7`, `b4`},
	`seventhsFlatThirdteenth`: {`1`, `3`, `5`, `b7`, `b6`},
	`addNine`:                 {`1`, `3`, `5`, `2`},
	`addFourth`:               {`1`, `3`, `5`, `4`},
	`addSixth`:                {`1`, `3`, `5`, `6`},
	`sixNine`:                 {`1`, `3`, `5`, `6`, `2`},
	`sevenSix`:                {`1`, `3`, `5`, `b7`, `6`},
	`suspendedSecond`:         {`1`, `2`, `5`},
	`suspendedFourth`:         {`1`, `4`, `5`},
}

var cof = map[string]int{
	`C`:  0,
	`G`:  1,
	`D`:  2,
	`A`:  3,
	`E`:  4,
	`B`:  5,
	`Cb`: 5,
	`F#`: 6,
	`Gb`: 6,
	`Db`: 7,
	`C#`: 7,
	`G#`: 8,
	`Ab`: 8,
	`D#`: 9,
	`Eb`: 9,
	`Bb`: 10,
	`F`:  11,
}

var relIntervals = map[Mode]string{
	Ionian:     `1`,
	Dorian:     `b7`,
	Phygrian:   `b6`,
	Lydian:     `5`,
	Mixolydian: `4`,
	Aeolian:    `b3`,
	Locrian:    `b2`,
}

// HarmonyModels represents the models of this package and provides functions to acces the models
var HarmonyModels = Models{}
